#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "fraction.h"

bruch flip(bruch a) {	//Zähler und Nenner tauschen
	uint64_t temp = a.zaeler;
	a.zaeler = a.nenner;
	a.nenner = temp;
	return a;
}

bruch make_bruch(float zahl) {
    bruch a;
    a.nenner = 1;
    a.vz = SIGN_PLUS;
    if(zahl<0){     //wenn zahl negativ, vorzeichen setzen und zahl positiv machen
        a.vz = SIGN_MINUS;
        zahl = -zahl;
    }
    uint64_t ganz = zahl;   //ganzzaligen Anteil bestimmen
    while(zahl != ganz){    //so lange mit 10 multiplizieren bis die Zahl ganzzahling ist.
        zahl *=10;
        a.nenner *=10;
        ganz = zahl;
   }
    a.zaeler = zahl;
    return a; 
}


void print_bruch(bruch a) {		//ausgabe eines Bruches in die Konsole
    if(a.nenner == 1) {
        if(a.vz==SIGN_MINUS) {
            printf("-%lu\n", a.zaeler); 
        }
        else {
            printf("%lu\n", a.zaeler);     //kein Vorzeichen bei Plus
        }
    }
    else {
        if(a.vz==SIGN_MINUS) {
            printf("-%lu/", a.zaeler); printf("%lu\n", a.nenner);
        }
        else {
            printf("%lu/", a.zaeler); printf("%lu\n", a.nenner);
        }
    }
}

void print_bruch_pointer(bruch *a) {
    if(a->nenner == 1) {
        if(a->vz==SIGN_MINUS) {
            printf("-%lu\n", a->zaeler); 
        }
        else {
            printf("%lu\n", a->zaeler);     //kein Vorzeichen bei Plus
        }
    }
    else {
        if(a->vz==SIGN_MINUS) {
            printf("-%lu/", a->zaeler); printf("%lu\n", a->nenner);
        }
        else {
            printf("%lu/", a->zaeler); printf("%lu\n", a->nenner);
        }
    }
}

bruch test_bruch(bruch a) { 	//test ob der Bruch gülltig ist
	if(a.nenner == 0){
		printf("do you even math, bro?\n");
		exit(1);
	}
	if(a.vz != SIGN_PLUS && a.vz != SIGN_MINUS) {
		printf("kein gueltiges Vorzeichen\n");
		exit(2);
		}
	return a;
}


bruch hauptnenner(bruch a, bruch b) {	//bringt einen Bruch a auf den gleichen Nenner wie b
	bruch a_new;
	a_new.zaeler = a.zaeler*b.nenner;
	a_new.nenner = a.nenner*b.nenner;
	a_new.vz = a.vz;
	return a_new;
}


uint64_t ggt(uint64_t a, uint64_t b) { 	// Algorithmus zum bestimmen des GGT
  uint64_t c;
  while ( b != 0 ) {
    c = a % b; a = b; b = c;
  }
  return a;
}

bruch kuerzen(bruch a) {		//Bruch kürzen
	uint64_t c = ggt(a.zaeler, a.nenner);
	a.zaeler /= c;
	a.nenner /= c;
	return a;
}

bruch multiplikation(bruch a, bruch b) {	//multiplikation von zwei Brüchen
	bruch c;
	c.zaeler = a.zaeler*b.zaeler;
	c.nenner = a.nenner*b.nenner;
	if(a.vz == b.vz) c.vz = SIGN_PLUS;	//+ wenn a und b gleiches Vorzeichen haben
	else c.vz = SIGN_MINUS;				//sonst -
	return kuerzen(c);
}

bruch *multiplikation_pointer(bruch *a, bruch *b) {	//multiplikation von zwei Brüchen
	bruch *result = (bruch*)malloc(sizeof(bruch));    
	result->zaeler = a->zaeler*b->zaeler;
	result->nenner = a->nenner*b->nenner;
	if(a->vz == b->vz) result->vz = SIGN_PLUS;	//+ wenn a und b gleiches Vorzeichen haben
	else result->vz = SIGN_MINUS;				//sonst -
	return result;
}

bruch division(bruch a, bruch b) {	//Division durch a * b^-1
	bruch c = multiplikation(a, test_bruch(flip(b)));
	return c;
}

bruch addition(bruch a, bruch b) {		//addiert zwei Brüche
	bruch aa = hauptnenner(a, b);
	bruch bb = hauptnenner(b, a);
	bruch c;

	//da aus verschiedenen Gründen der Zähler unsigned sind brauchen wir hier ein paar Fallunterscheidungen.

	if(aa.vz == SIGN_PLUS && bb.vz == SIGN_PLUS) {
		c.zaeler = aa.zaeler+bb.zaeler;
		c.vz = SIGN_PLUS;
	}
	else if(aa.vz == SIGN_MINUS && bb.vz == SIGN_MINUS) {
		c.zaeler = aa.zaeler+bb.zaeler;
		c.vz = SIGN_MINUS;
	}
	else if(aa.vz == SIGN_MINUS && bb.vz == SIGN_PLUS) {
		if(bb.zaeler >= aa.zaeler) {
			c.zaeler = bb.zaeler-aa.zaeler;
			c.vz = SIGN_PLUS;
		}
		else {
			c.zaeler = aa.zaeler-bb.zaeler;
			c.vz = SIGN_MINUS;
		}
	}
	else {
		if(aa.zaeler >= bb.zaeler) {
			c.zaeler = aa.zaeler-bb.zaeler;
			c.vz = SIGN_MINUS;
		}
		else {
			c.zaeler = bb.zaeler-aa.zaeler;
			c.vz = SIGN_MINUS;
		}
	}
	c.nenner = aa.nenner;
	return kuerzen(c);
}



bruch subtraktion(bruch a, bruch b) {		//subtraktion zweier Brüche durch Vorzeichenwechsel des zweiten und Addition
	bruch bb = b;
	if(bb.vz == SIGN_PLUS) {
		bb.vz = SIGN_MINUS;
		return addition(a, bb);
	}
	else {
		bb.vz = SIGN_PLUS;
		return addition(a, bb);
	}
}


/*
int main() {	//test


	bruch a = {SIGN_MINUS, 1, 2}; test_bruch(a); printf("a: "); print_bruch(kuerzen(a));
	bruch b = {SIGN_PLUS, 2, 3}; test_bruch(b); printf("b: "); print_bruch(kuerzen(b));
	printf("a*b: "); print_bruch(kuerzen(multiplikation(a, b)));
	printf("a/b: "); print_bruch(kuerzen(division(a, b)));
	printf("a+b: "); print_bruch(kuerzen(addition(a, b)));
	printf("a-b: "); print_bruch(kuerzen(subtraktion(a, b)));
    print_bruch(kuerzen(make_bruch(0.75)));
    }
    */
/*
int main() {
    bruch *p1;
    bruch *p2;
    bruch *p3;
    p1 = (bruch*)malloc(sizeof(bruch));
    p2 = (bruch*)malloc(sizeof(bruch));
    p3 = (bruch*)malloc(sizeof(bruch));
    
    *p1={0,2,8};
    *p2={0,1,4};
    
    *p3=multiplikation(*p1, *p2);
    print_bruch_pointer(p3);
}    
*/