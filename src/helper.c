#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "helper.h"

void *safealloc(size_t size) {
    void *mem;

    // Quit the program here if the memory allocation fails
    if(!(mem = malloc(size))) {
        printf("Memory allocation failed!");
        exit(EXIT_FAILURE);
    }

    // Null the memory so we get predictable results
    memset(mem, 0, size);

    return mem;
}

void *saferealloc(void *ptr, size_t size) {
    void *mem;

    // Quit the program here if the memory reallocation fails
    if(!(mem = realloc(ptr, size))) {
        printf("Memory allocation failed!");
        exit(EXIT_FAILURE);
    }

    return mem;
}
