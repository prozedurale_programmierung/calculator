

#ifndef __FRACTION_H_
#define __FRACTION_H_

#include <stdint.h>

// These are the possible values of the sign bit
#define SIGN_PLUS 0
#define SIGN_MINUS 1

typedef struct {
    char vz;    //Vorzeichen
    uint64_t zaeler;
    uint64_t nenner;
} bruch;

// User input/output
void print_bruch(bruch a);
void print_bruch_pointer(bruch *a);
bruch test_bruch(bruch a);
bruch make_bruch(float zahl);

// Basic arithmetics
bruch addition(bruch a, bruch b);
bruch subtraktion(bruch a, bruch b);
bruch multiplikation(bruch a, bruch b);
bruch *multiplikation_pointer(bruch *a, bruch *b);
bruch division(bruch a, bruch b);

// Fraction helpers
bruch kuerzen(bruch a);                 // Cancel/shorten a fraction
bruch hauptnenner(bruch a, bruch b);    // Get the least common denominator
bruch flip(bruch a);                    // Exchange numerator and denominator
uint64_t ggt(uint64_t a, uint64_t b);   // Get the greatest common divisor

#endif

