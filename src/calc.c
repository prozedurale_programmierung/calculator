#include <stdio.h>
#include <ctype.h>
#include <stdint.h>

#include "helper.h"
#include "lexer.h"
#include "parser.h"
#include "evaluate.h"
#include "fraction.h"

/* This function does nothing but formatting an expression and it's sub-expressions
 * into an s-expression, so likely the same thing as the input was. Currently
 * it is only for debugging-reasons, but it might be interesting to extend this
 * to use it e.g. for tests (compare input with parser output) or auto-formatting
 * (removing unnecessary whitespaces, …) from the input.
 */
void debug_expression(expression *expr) {
    if(expr->type == EXPR_LITERAL) {
        printf("%s ", expr->literal);
    } else if(expr->type == EXPR_FUNCTION) {
        printf("(%s ", expr->identifier);

        for(size_t i = 0; i < expr->childc; i++) {
            debug_expression(expr->childs[i]);
        }

        printf("\b) ");
    }
}


//create a struct bruch from the input
//maybe move to a better place

value *func_make_bruch(value **values, size_t valuec) {
    bruch *result = safealloc(sizeof(bruch));
    int *numerator = (int*)safealloc(sizeof(int));
    int *denominator = (int*)safealloc(sizeof(int));
    char *raw;
    
    switch(values[0]->type) {   //test whether the first argument is an integer
        case VALUE_INT: 
            //first expression is the numerator.
            numerator = (int*)values[0]->content;
            break;
           
        case VALUE_UNKNOWN:
            raw = (char*)values[0]->content;
            if(!isdigit(*raw)) {
            //unsave method, crazy shit if a minus sign isn't followed by a number
                if(*raw=='-'){ 
                    *numerator = atoi(raw);
                }
                else {
                    EVAL_ERROR("%s is not numeric!", raw);
                }
            }
            else {
                *numerator = atoi(raw);
            }
            break;
            default:
                EVAL_ERROR("Unsupported data type %d", values[0]->type);        
    }
            if(*numerator < 0) { // If the numerator is negative, make it positive and set the sign to minus   
                *numerator = -*numerator;
                result->vz = SIGN_MINUS;
            }
            //set the numertor
            result->zaeler = *numerator;
    
             *denominator = 1;

            for(size_t i = 1; i < valuec; i++) {
                switch(values[i]->type) {   //test evry argument
                    case VALUE_INT:
                        *denominator *= *(int*)values[i]->content;   //create the denominator (a/b/c/d = a/b*c*d)
                        break;
                    case VALUE_UNKNOWN:
                        raw = (char*)values[i]->content;
                        if(!isdigit(*raw)) {
                            if(*raw=='-'){
                    
                            *denominator *= atoi(raw);
                            }
                            else {
                                EVAL_ERROR("%s is not numeric!", raw);
                            }
                        }
                        else {
                            *denominator *= atoi(raw);
                        }
                        break;
                    default:
                        EVAL_ERROR("Unsupported data type %d", values[i]->type);
            }
            }      
    
    if(*denominator < 0) {  //check if denominator in negativ
        if(result->vz == SIGN_MINUS) {  //if fraction is already negative (nummerator was negative)
            result->vz = SIGN_PLUS;
            *denominator = -*denominator;
        }
        else {
            result->vz = SIGN_MINUS;
            *denominator = -*denominator;
        }
    }
    //set the denominator 
    result->nenner = *denominator;
   
    // Store the result in a value of an appropriate type
    value *res = create_value(VALUE_FRAC);
    res->content = result;
    return res;
}

value *func_add_bruch(value **values, size_t valuec) {
    bruch *result = safealloc(sizeof(bruch));
    result->vz = SIGN_PLUS; result->zaeler = 0; result->nenner = 1; //default value is 0
    
    for(size_t i = 0; i < valuec; i++) {
        // only support fraction types
        switch(values[i]->type) {
            case VALUE_FRAC:
                *result = addition(*result, *(bruch*)values[i]->content);
                break;
            default:
                EVAL_ERROR("Unsupported data type %d", values[i]->type);
        }
    }
    value *res = create_value(VALUE_FRAC);
    res->content = result;
    return res;    
}

//this function supports only 2 arguments yet

value *func_subtract_bruch(value **values, size_t valuec) {
    //printf("HI");
    bruch *result = safealloc(sizeof(bruch));
    /*result->vz = SIGN_PLUS; result->zaeler = 0; result->nenner = 1; //default value is 0
    
    for(size_t i = 0; i < valuec; i++) {
        // only support fraction types
        switch(values[i]->type) {
            case VALUE_FRAC:
                *result = subtraktion(*result, *(bruch*)values[i]->content);
                break;
            default:
                EVAL_ERROR("Unsupported data type %d", values[i]->type);
        }
    } */
    
    if(valuec !=2) {
        EVAL_ERROR("wrong numer of arguments %zu", valuec);
    }
    
    switch(values[0]->type) {
            case VALUE_FRAC:
            //printf("1 ok\n");
                break;
            default:
                EVAL_ERROR("Unsupported data type %d", values[0]->type);
    }
    switch(values[1]->type) {
            case VALUE_FRAC:
              //  printf("2 ok\n");
                break;
            default:
                EVAL_ERROR("Unsupported data type %d", values[1]->type);
    }
    
    *result = subtraktion(*(bruch*)values[0]->content, *(bruch*)values[1]->content);
    
    value *res = create_value(VALUE_FRAC);
    res->content = result;
    return res;    
}

value *func_multiply_bruch(value **values, size_t valuec) {
    bruch *result = safealloc(sizeof(bruch));
    result->vz = SIGN_PLUS; result->zaeler = 1; result->nenner = 1; //default is 1
    
    for(size_t i = 0; i < valuec; i++) {

        // only support fraction types
        switch(values[i]->type) {
            case VALUE_FRAC:
                *result = multiplikation(*result, *(bruch*)values[i]->content);
                break;
            default:
                EVAL_ERROR("Unsupported data type %d", values[i]->type);
        }
    }
    value *res = create_value(VALUE_FRAC);
    res->content = result;
    return res;    
}

value *func_divide_bruch(value **values, size_t valuec) {
    bruch *result = safealloc(sizeof(bruch));
   /* result->vz = SIGN_PLUS; result->zaeler = 1; result->nenner = 1; //default is 1
    
    for(size_t i = 0; i < valuec; i++) {

        // only support fraction types
        switch(values[i]->type) {
            case VALUE_FRAC:
                *result = division(*result, *(bruch*)values[i]->content);
                break;
            default:
                EVAL_ERROR("Unsupported data type %d", values[i]->type);
        }
    } */
    
    if(valuec !=2) {
        EVAL_ERROR("wrong numer of arguments %zu", valuec);
    }
    
    switch(values[0]->type) {
            case VALUE_FRAC:
            //printf("1 ok\n");
                break;
            default:
                EVAL_ERROR("Unsupported data type %d", values[0]->type);
    }
    switch(values[1]->type) {
            case VALUE_FRAC:
              //  printf("2 ok\n");
                break;
            default:
                EVAL_ERROR("Unsupported data type %d", values[1]->type);
    }
    
    *result = division(*(bruch*)values[0]->content, *(bruch*)values[1]->content);
    
    value *res = create_value(VALUE_FRAC);
    res->content = result;
    return res;    
}

int main() {
    // 1.   Gather input: We just store it in the source for testing purposes
    char input[500];
    printf("calc> "); //enter cooler mesage here
    fgets(input, 500, stdin); //is this safe??
    printf("\033[1mInput is:\x1B[0m %s\n\n", input);

    // 2.   Create a tokenizer on the input
    tokenizer *tz = create_tokenizer(input);

    // 3.   Parse the tokenized input into an expression tree of functions and literals
    parser *pr = create_parser(tz);
    expression *expr = parser_do(pr);

    // 4.   Create an evaluator that can evaluate the expression tree
    evaluator *ev = create_evaluator(expr);

    // 4.1. Register functions
    function *bruch = create_function("b");
    bruch->func = &func_make_bruch;
    evaluator_register_function(ev, bruch);
    
    function *plus = create_function("+");
    plus->func = &func_add_bruch;
    evaluator_register_function(ev, plus);
    
    function *mal = create_function("*");
    mal->func = &func_multiply_bruch;
    evaluator_register_function(ev, mal);
    
    function *minus = create_function("-");
    minus->func = &func_subtract_bruch;
    evaluator_register_function(ev, minus);
    
    function *durch = create_function("/");
    durch->func = &func_divide_bruch;
    evaluator_register_function(ev, durch);

    // 4.2. Do the actual evaluation
    value *result = evaluator_do(ev);

    // 5.   Print out the result in a nice way
    printf("\n\033[1mResult is:\x1B[0m ");
    value_print(result);
}
