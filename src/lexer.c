#include <stdlib.h>
#include <stdio.h>

#include "helper.h"
#include "lexer.h"

token *create_token() {
    token *t = safealloc(sizeof(token));
    t->type = L_NOTHING;
    return t;
}


tokenizer *create_tokenizer(char *fp) {
    tokenizer *tz = safealloc(sizeof(tokenizer));
    tz->fp = fp;

    TRACELN("Tokenizer instance created");

    return tz;
}

int tokenizer_unread(tokenizer *tz) {
    TRACELN("Buffer position moved one back");
    return --tz->pos;
}

token *tokenizer_read(tokenizer *tz) {
    token *t = create_token();

    // TODO What happens if a token exceeds the buffer length? -> realloc
    char buffer[255];
    int used_buffer = 0;

    /* This loop runs until the logic inside decided that we gathered enough
     * input for this token type. For example names and fractions might have
     * more than one character per token.
     */
    while(1) {
        char cur = tz->fp[tz->pos++];
        TRACELN("Buffer position moved one forward");

        // \0 denotes the end of user input
        if(cur == '\0') {
            TRACELN("Found literal end");
            tokenizer_unread(tz);
            goto BREAKLOOP;
        }

        /* Closing parenthesis MAY denote the end of the previous token if type
         * is not L_NOTHING. Then we have to unread to process the parenthesis within
         * the next tokenizer_read()-call and break the loop.
         */
        if(cur == ')' && t->type != L_NOTHING) {
            TRACELN("Found expression end");
            tokenizer_unread(tz);
            goto BREAKLOOP;
        }

        /* Whitespaces, newlines and tabs MAY denote the end of a single token.
         * As long as we have not read anything by now (type is L_NOTHING), we just
         * continue to skip these token delimiters.
         */
        if(cur == ' ' || cur == '\n' || cur == '\t') {
            if(t->type == L_NOTHING) {
                TRACELN("Skipping whitespace");
                continue;
            }

            TRACELN("Found token separator");
            tokenizer_unread(tz);
            goto BREAKLOOP;
        }

        // Classify the current character, fill the buffer
        switch(cur) {
            case '(':
                TRACELN("Found expression opening");
                t->type = L_OPEN;
                buffer[used_buffer++] = cur;
                goto BREAKLOOP;
            case ')':
                TRACELN("Found expression closing");
                t->type = L_CLOSE;
                buffer[used_buffer++] = cur;
                goto BREAKLOOP;
            default:
                /* Everything else is an arbitrary string, thus we just continue
                 * parsing until there is a token delimiter (see above)
                 */
                TRACELN("Found arbitrary string");
                t->type = L_STRING;
                buffer[used_buffer++] = cur;
        }
    }

BREAKLOOP:

    // Write the used part of the buffer into a correctly sized char*
    t->raw = safealloc((used_buffer+1)*sizeof(char));
    for(int i = 0; i < used_buffer; i++) {
        t->raw[i] = buffer[i];
    }
    t->raw[used_buffer+1] = '\0';

    return t;
}

