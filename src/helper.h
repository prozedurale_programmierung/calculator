/**
 * @file helper.h
 * @brief Contains helper functions for debugging, memory management, …
 * @author Leonard Techel
 */
#ifndef __HELPER_H_
#define __HELPER_H_

#if defined DEBUG && DEBUG == 1
/**
 * @def TRACE(format, ...)
 * @brief Print a formatted debug trace including file and line occurence.
 *
 * Use printf-style format strings for @a format. Please not that you need at
 * least one argument after the format string in order to use this. Consider
 * using TRACELN(content) instead!
 */
#define TRACE(format, ...) printf("\033[1mDebug:\x1B[0m %s:%d: " format "\n", __FILE__, __LINE__, __VA_ARGS__);
#else
#define TRACE(format, ...)
#endif

/**
 * @def TRACELN(content)
 * @brief Print a single line trace.
 */
#define TRACELN(content) TRACE("%s", content);

/**
 * @brief Safe malloc. Directly exit the program if the memory allocation fails.
 * @param size Amount of memory to be allocated
 * @return Pointer to the allocated memory
 */
void *safealloc(size_t size);

/**
 * @brief Safe realloc. Directly exit the program if the memory reallocation fails.
 * @param ptr Pointer to current memory
 * @param size Amount of memory to be reallocated
 * @return Pointer to the reallocated memory. Continue using this.
 */
void *saferealloc(void *ptr, size_t size);

#endif
