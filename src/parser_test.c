#include <stdio.h>

#include "test.h"
#include "parser.h"

/* TODO PARSER TESTS
 *
 * PLEASE TRY to avoid using the lexer! We can just manually create test cases
 * using the tokenizer and token struct.
 *
 * These test should primarily check if the parser correctly takes care of the
 * token order. Furthermore, it should verify that the parser works in a
 * recursive way.
 *
 * To test the parser, we should somehow compare data structures. The parser
 * creates a tree consisting of expressions and values.
 *
 * Expected positive test cases:
 *
 * * Expression without any attributes:
 *      (foo)
 * * Expression with one attribute:
 *      (foo 3/4)
 * * Expression with multiple attributes:
 *      (foo 3/4 5)
 * * Expression with subexpression after the name:
 *      (foo (bar) 5)
 * * Expression with subexpression after a attribute:
 *      (foo 3/4 (bar) 5)
 *
 * Expected negative test cases:
 *
 * * Expression without a name:
 *      ()
 * * Expression without closing parenthesis:
 *      (foo 3/4 5
 * * Expression with randomly missing closing parenthesis:
 *      (foo (bar 5 6)
 */
