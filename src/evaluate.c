#include <stdio.h>
#include <string.h>

#include "helper.h"
#include "parser.h"
#include "evaluate.h"
#include "fraction.h"

value *create_value(int type) {
    value *val = safealloc(sizeof(value));
    val->type = type;
    return val;
}

void value_from_literal(value *val, expression *expr) {
    if(expr->type != EXPR_LITERAL) {
        EVAL_ERROR("Expression %s is not a literal!", expr->identifier);
    }

    val->content = expr->literal;
}

/* This is the place to add logic for printing out a data type. So PLEASE add
 * something here if you add a new data type!
 */
void value_print(value *val) {
    switch(val->type) {
        case VALUE_UNKNOWN:
            printf("%s\n", (char*)val->content);
            break;
        case VALUE_INT:
            printf("%d\n", *(int*)val->content);
            break;
        case VALUE_FRAC:
            print_bruch_pointer((bruch*)val->content);
            break;
            
        default:
            printf("Error printing out a value of type %d\n", val->type);
    }
}


function *create_function(char *identifier) {
    function *func = safealloc(sizeof(function));
    func->identifier = identifier;
    return func;
}


evaluator *create_evaluator(expression *expr) {
    evaluator *ev = safealloc(sizeof(evaluator));
    ev->expr = expr;
    return ev;
}

void evaluator_register_function(evaluator *ev, function *f) {
    ev->functionc++;

    ev->functions = saferealloc(ev->functions, ev->functionc*sizeof(function*));
    ev->functions[ev->functionc-1] = f;

    TRACE("Function %s registered", f->identifier);
}

function *evaluator_get_function(evaluator *ev, char *identifier) {
    TRACE("Requested function %s", identifier);

    // TODO implement this using a hashmap for performance reasons
    for(size_t i = 0; i < ev->functionc; i++) {
        // Continue if the identifiers are not equal
        if(strcmp(ev->functions[i]->identifier, identifier) != 0) continue;

        // Otherwise return the function
        TRACE("Found function %s", ev->functions[i]->identifier);
        return ev->functions[i];
    }

    TRACE("Function %s not found", identifier);
    return NULL;
}

/* The actual evaluation logic happens HERE. Calling evaluator_do will result
 * in calling this recursive function with the initial, outer-most expression.
 *
 * As seen in parser.h, there are two types of expressions: Literals and
 * functions. While evaluating, literals are "evaluated" into values of unknown
 * type that get passed to functions which then compute a new value of a more
 * specific type which gets passed to their parent expressions and so forth
 * until the outer-most expression is evaluated.
 */
value *evaluator_do_inner(evaluator *ev, expression *expr) {
    // Return literal expressions as unknown value
    if(expr->type == EXPR_LITERAL) {
        TRACE("Evaluating literal %s", expr->literal);

        value *val = create_value(VALUE_UNKNOWN);
        value_from_literal(val, expr);
        return val;
    }

    // Evaluate function expressions
    if(expr->type == EXPR_FUNCTION) {
        TRACE("Evaluating function %s", expr->identifier);

        // First evaluate all child-expressions
        value **values = safealloc(expr->childc*sizeof(value*));
        for(size_t i = 0; i < expr->childc; i++) {
            TRACE("Processing child %zu of %s", i, expr->identifier);

            value *val = evaluator_do_inner(ev, expr->childs[i]);
            if(!val) {
                EVAL_ERROR("Evaluation of child %zu (%s) of expression %s failed", i, expr->childs[i]->identifier, expr->identifier);
            }

            values[i] = val;
        }

        // Then evaluate the expression with it's already evaluated children
        function *f = evaluator_get_function(ev, expr->identifier);
        if(!f) {
            EVAL_ERROR("Unknown function \"%s\"", expr->identifier);
        }

        return (*f->func)(values, expr->childc);
    }

    return NULL;
}

value *evaluator_do(evaluator *ev) {
    value *val = evaluator_do_inner(ev, ev->expr);
    if(!val) {
        EVAL_ERROR("Evaluation of expression %s failed", ev->expr->identifier);
    }

    return val;
}
