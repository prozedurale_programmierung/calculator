/**
 * @file evaluate.h
 * @brief Evaluates an expression completely.
 * @author Leonard Techel
 *
 * Evaluation works by either recursively evaluating the children of an expression
 * into a value or converting literal expressions directly into values.
 *
 * This file also contains data types and helper functions related to values
 * with data types and functions that can be evaluated.
 */

#ifndef __EVALUATE_H_
#define __EVALUATE_H_

#include <stdlib.h>

#include "parser.h"

/**
 * @def EVAL_ERROR(format, ...)
 * @brief Print out an evaluation error, @c exit(EXIT_FAILURE); the program
 *
 * Format string must be given as @a format. VA_ARGS functions have to have at
 * least one argument, so in order to print a simple string consider doing
 * something like:
 *
 * @code{.c}
 * EVAL_ERROR("%s", "foo bar baz");
 * @endcode
 */
#define EVAL_ERROR(format, ...) printf("\033[1m\x1B[31mEvaluation error:\x1B[0m " format "\n", __VA_ARGS__); exit(EXIT_FAILURE);

/**
 * @defgroup valueTypes Value type constants
 * @brief Possible types for a #value struct
 *
 * These data types are checked by the function evaluating a value itself. There
 * is no prior type checking before calling the function!
 *
 * To add a new data type, make sure that:
 * 1. You add the corresponding constant HERE
 * 2. You add a corresponding switch statement and printing instructions into
 *    value_print()
 *
 * @{
 */
#define VALUE_UNKNOWN 0 //!< Initial data type. Needs to be evaluated by the called function. Content is of type (int*)
#define VALUE_INT 1     //!< Integer data type. Content is of type (int*)
#define VALUE_FRAC 2 	//!<Fraction data type 
/** @} */

/**
 * @brief Contains a value object
 *
 * This is what a function gets as parameters and uses as a return value.
 */
typedef struct {
    int type;      /**< Indicates the content type of the pointer. See  */
    void *content; /**< Pointer to the value content. */
} value;

/**
 * @brief Creates a new value instance
 * @param type See @ref valueTypes
 * @return Pointer to a value instance
 */
value *create_value(int type);

/**
 * @brief Parse a literal expression into a value
 * @param val The value pointer into which the expression should get parsed
 * @param expr The expression to be parsed
 *
 * If the expression is not EXPR_LITERAL, the evaluator will stop!
 */
void value_from_literal(value *val, expression *expr);

/**
 * @brief Print out a value on the screen
 * @param val The value instance to work on
 */
void value_print(value *val);


/**
 * @brief Contains a function
 *
 * PLEASE NOTE that there are neither prior type nor value count compatibility
 * checks which means that you actually have to implement this on your own!
 *
 * Possible data types are specified by @ref valueTypes. If you receive a value
 * of type VALUE_UNKNOWN which is the initial, unprocessed state, you either have
 * to reject this as value or convert it into something useful.
 *
 * If your function encounters ANY error while evaluating (types wrong, …), you
 * HAVE to call EVAL_ERROR(format, ...) so the evaluation stops displaying an
 * error to the user.
 */
typedef struct {
    char *identifier; /**< The name of the function */
    value *(*func)(value**, size_t valuec); /**< The function receives an array of arguments */
} function;

/**
 * @brief Create a new function instance
 * @return A function instance
 */
function *create_function(char *identifier);


/**
 * @brief Contains an evaluator instance
 */
typedef struct {
    expression *expr;     /**< The expression that should be evaluated */
    size_t functionc;     /**< Available function count */
    function **functions; /**< Available functions */
} evaluator;

/**
 * @brief Create a new evaluator instance
 * @param expr Expression to be evaluated
 * @return An evaluator instance
 */
evaluator *create_evaluator(expression *expr);

/**
 * @brief Register a function at the evaluator
 * @param ev The evaluator instance to work on
 * @param f The function to be registered
 *
 * Registering a function means making it available to the evaluator to be used
 * when evaluating
 */
void evaluator_register_function(evaluator *ev, function *f);

/**
 * @brief Get a function by it's identifier (name)
 * @param ev The evaluator instance to work on
 * @param identifier The function identifier / name
 * @return The function or a NULL pointer if there is no function identified by this identifier
 */
function *evaluator_get_function(evaluator *ev, char *identifier);

/**
 * @brief Recursively evaluate
 * @param ev The evaluator instance to work on
 * @return The computed value
 */
value *evaluator_do(evaluator *ev);

#endif
