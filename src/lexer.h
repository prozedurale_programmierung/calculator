/**
 * @file lexer.h
 * @brief Tokenizes the input for further processing.
 * @author Leonard Techel
 *
 * Usually you just skip this and take a look at parser.h.
 *
 * The lexer works by holding a buffer on the input (might be later on changed
 * to FILE) and then just peeking around by walking over each byte/character
 * and then classifying and grouping the input to tokens.
 */

#ifndef __LEXER_H_
#define __LEXER_H_

/**
 * @defgroup tokenTypes Lexer token type constants
 * @brief Possible types for a #token struct
 * @{
 */
#define L_NOTHING -1 //!< Initial state; Nothing tokenized
#define L_OPEN 0     //!< Opening parenthesis: (
#define L_CLOSE 1    //!< Closing parenthesis: )
#define L_STRING 2   //!< Arbitrary string, e.g. function identifier, literal, …
/** @} */

/**
 * @brief Contains a token
 */
typedef struct {
    int type;   /**< See @ref tokenTypes */
    char *raw;  /**< The raw token string */
} token;

/**
 * @brief Create a new token instance
 * @return A token instance
 */
token *create_token();

/**
 * @brief Contains the state of a tokenizer, respectively it's instance
 *
 * All functions working on a tokenizer instance somehow need to know the current
 * buffer to work on.
 *
 * The buffer MAY later on change to a FILE instance.
 */
typedef struct {
    char *fp; /**< The user input string to work on */
    size_t pos;  /**< Current buffer position */
} tokenizer;

/**
 * @brief Create a new tokenizer instance
 * @param fp The user input string to work on
 * @return A tokenizer instance
 */
tokenizer *create_tokenizer(char *fp);

/**
 * @brief Set the buffer one position back
 * @param tz The tokenizer instance to work on
 * @return The new buffer position
 */
int tokenizer_unread(tokenizer *tz);

/**
 * @brief Get the next token from the buffer
 * @param tz The tokenizer instance to work on
 * @return The next token object
 */
token *tokenizer_read(tokenizer *tz);

#endif
