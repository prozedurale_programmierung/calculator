#include <stdlib.h>
#include <stdio.h>

#include "test.h"
#include "../src/lexer.h"

/* Compares the types of tokens in an array of tokens with an array of expected
 * token types. They HAVE to have the exact same order and length.
 */
error check_token_types(token *tokens, int tokens_size, int *expected_types, int expected_types_size) {
    error err;
    err.message = malloc(sizeof(char)*80);

    if(tokens_size != expected_types_size) {
        err.success = 0;
        sprintf(err.message, "Less tokens than expected: %d vs %d !\n", tokens_size, expected_types_size);
        return err;
    }

    for(int i = 0; i < tokens_size; i++) {
        if(tokens[i].type != expected_types[i]) {
            err.success = 0;
            sprintf(err.message, "Token no. %d: %d does not match %d!\n", i, tokens[i].type, expected_types[i]);
            return err;
        }
    }

    err.success = 1;
    return err;
}

/* Tokenizes a string and checks if the returned token types match what we expect
 */
void check_tokenizer(char *input, int *expected_tokens, int expected_tokens_size) {
    tokenizer *tz = create_tokenizer(input);

    token tokenbuffer[255];
    int used_tokenbuffer = 0;

    // Somehow this is hell if we do just `token t;
    token *t;
    while(1) {
        t  = tokenizer_read(tz);

        if(t->type == L_NOTHING) break;
        tokenbuffer[used_tokenbuffer++] = *t;
    }

    error err = check_token_types(tokenbuffer, used_tokenbuffer, expected_tokens, expected_tokens_size);
    if(err.success != 1) {
        printf("%sERROR:%s for %s: %s\n", COLOR_RED, COLOR_RESET, input, err.message);
        return;
    }
    printf("%sSuccess%s for %s!\n", COLOR_GREEN, COLOR_RESET, input);

    free(t);
}

void test_lexer() {
    printf("\nTesting the lexer ...\n\n");

    // Simple
    int e1[6] = {L_OPEN, L_STRING, L_STRING, L_STRING, L_STRING, L_CLOSE};
    check_tokenizer("(foo 3/4 4 bar)", e1, 6);

    // Unexpected spaces
    int e2[5] = {L_OPEN, L_STRING, L_STRING, L_STRING, L_CLOSE};
    check_tokenizer("( foo 3/4     5 )", e2, 5);

    // Newlines
    int e3[5] = {L_OPEN, L_STRING, L_STRING, L_STRING, L_CLOSE};
    check_tokenizer("(foo\n3/4\n5)", e3, 5);

    // Tabs
    int e4[4] = {L_OPEN, L_STRING, L_STRING, L_CLOSE};
    check_tokenizer("(foo\t3/4)", e4, 4);

    // Nested
    int e5[9] = {L_OPEN, L_STRING, L_STRING, L_OPEN, L_STRING, L_STRING, L_CLOSE, L_STRING, L_CLOSE};
    check_tokenizer("(foo 2/3\n\t(bar 4/5 )\n3)", e5, 9);

    // double-nested
    int e6[14] = {L_OPEN, L_STRING, L_STRING, L_OPEN, L_STRING, L_STRING, L_OPEN, L_STRING, L_STRING, L_CLOSE, L_STRING, L_CLOSE, L_STRING, L_CLOSE};
    check_tokenizer("(foo 1 (bar 2 (baz 3) 4) 5)", e6, 14);
}
