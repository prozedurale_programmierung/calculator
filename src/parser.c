#include <stdlib.h>
#include <stdio.h>

#include "helper.h"
#include "parser.h"
#include "lexer.h"

expression *create_expression(int type) {
    expression *expr = safealloc(sizeof(expression));
    expr->type = type;
    return expr;
}

void expression_add_child(expression *expr, expression *child) {
    if(expr->type != EXPR_FUNCTION) {
        PARSE_ERROR("%s", "Tried to add a sub-expression to something else than a function expression");
        exit(EXIT_FAILURE);
    }

    expr->childc++;

    expr->childs = saferealloc(expr->childs, expr->childc*sizeof(expression*));
    expr->childs[expr->childc-1] = child;
}

void expression_set_literal(expression *expr, char *literal) {
    if(expr->type != EXPR_LITERAL) {
        PARSE_ERROR("%s", "Tried to add a literal to something else than a literal expression");
        exit(EXIT_FAILURE);
    }

    expr->literal = literal;
}


parser *create_parser(tokenizer *tz) {
    parser *pr = safealloc(sizeof(parser));

    pr->tz = tz;
    pr->state = STATE_NOTHING;

    TRACELN("Parser instance created");

    return pr;
}

expression *parser_do(parser *pr) {
    expression *expr = create_expression(EXPR_FUNCTION);

    token *t;
    int state = STATE_NOTHING;

    /* Run the state machine as long as we have not found an error
     */
    TRACELN("Start parsing an expression");
    while(1) {
        t = tokenizer_read(pr->tz);

        TRACE("Current token: %s", t->raw);

        /* This is a simple state machine. Depending on the state of the previous
         * loop iteration, we can determine which token HAS or MAY follow next.
         * For example, on an opening parenthesis always follows the name of
         * the function.
         */
        switch(state) {
            // The initial state before the first loop iteration
            case STATE_NOTHING:
                // Next HAS to be opening parenthesis
                if(t->type == L_OPEN) {
                    TRACELN("Nothing parsed yet, expected L_OPEN found");

                    state = STATE_OPEN;
                    break;
                }

                PARSE_ERROR("Expected (, found \"%s\"", t->raw);
                break;

            /* After we found the opening parenthesis, there HAS to be the
             * expression name
             */
            case STATE_OPEN:
                // Next HAS to be a string
                if(t->type == L_STRING) {
                    TRACELN("Previous was expression opening, expected L_STRING as the function identifier found");

                    state = STATE_LITERAL;
                    expr->identifier = t->raw;
                    break;
                }

                PARSE_ERROR("Expected a name, found \"%s\"", t->raw);
                break;

            /* After we found the expression name, there are three options what
             * MAY comes next
             */
            case STATE_LITERAL:
                // Next MAY be a string --> simple argument
                if(t->type == L_STRING) {
                    TRACELN("Found literal");

                    // Create a literal expression
                    expression *literal = create_expression(EXPR_LITERAL);
                    expression_set_literal(literal, t->raw);

                    expression_add_child(expr, literal);
                    break;
                }

                // Next MAY be an opening parenthesis which marks a subexpression
                if(t->type == L_OPEN) {
                    TRACELN("Found expression opening, start parsing a subexpression");

                    tokenizer_unread(pr->tz);

                    // Recursively parse the sub-expression on the same parser/tokenizer
                    expression *nested_expr = parser_do(pr);
                    expression_add_child(expr, nested_expr);
                    break;
                }

                // Next MAY be a closing parenthesis which marks an expression end
                if(t->type == L_CLOSE || t->type == L_NOTHING) {
                    TRACELN("Found end of current expression");
                    goto BREAKLOOP;
                }

                PARSE_ERROR("Expected an attribute, found \"%s\"", t->raw);
                break;
        }
    }

BREAKLOOP:

    return expr;
}

