#ifndef __TEST_H_
#define __TEST_H_

#define COLOR_GREEN "\x1B[32m"
#define COLOR_RED   "\x1B[31m"
#define COLOR_RESET "\x1B[0m"

typedef struct {
    int success;
    char *message;
} error;

void test_lexer();

#endif
