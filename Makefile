.PHONY: all debug clean

CC = gcc
LD = $(CC)

override CFLAGS+=-std=c99 -pedantic -Wall -Wextra
override LDFLAGS+=-lm

OBJS = helper.o lexer.o parser.o evaluate.o fraction.o
TESTOBJS = lexer_test.o parser_test.o

all: calc

debug: CFLAGS+=-DDEBUG=1 -g
debug: calc test

calc: $(OBJS) calc.o
	$(LD) $(LDFLAGS) -o $@ $^

test: $(OBJS) $(TESTOBJS) test.o
	$(LD) $(LDFLAGS) -o $@ $^

%.o: src/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	$(RM) $(OBJS) calc.o calc
	$(RM) $(TESTOBJS) test.o test
