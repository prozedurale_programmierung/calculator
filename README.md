# LISP-Style calculator using fractions

## Usage
TODO

## Development
Please install the [EditorConfig](http://editorconfig.org/) plugin for your
desired editor! There is a `.editorconfig`-file to automatically configure your
editor correctly!

`src/` contains all C source code files.

Run the following to compile and start the program:

	make calc
	./calc

File structure is as follows:

**Generally useful:**
* `helper.c` contains helper functions for debugging, memory management, …

**Related to the interpreter core:**
* `calc.c` contains the main function
* `lexer.c` contains an input tokenizer
* `parser.c` contains a parser working on the tokenizer
* `evaluate.c` contains functions to evaluate the expression tree from the parser

**Related to functions that the evaluator calls:**
* `fraction.c` contains helpers to calculate with fractions

Documentation should be done in Doxygen-Style inside the header files. So do not
expect too much meta-comments inside the `.c`-files and instead open the header
files in a split screen or so!

Structs are, as long as possible, always passed around as pointers and initially
allocated using safealloc. Have a look at the instance/object creation functions!

### Documentation
Most of the code is extensively documented using [Doxygen](http://www.stack.nl/~dimitri/doxygen/).
To generate the documentation, run something like:

	cd docs/
	doxygen Doxyfile

### Software tests, debugging
There are some tests to ensure that e.g. the lexer and parser work as expected.
In addition it is possible to get extensive internal traces from the program.

To compile software tests and enable the debugging output, run something like:

	make debug
	./test
	./calc

Tests are programmed the following way:

* `foo_test.c` contains tests specific to a .c-file called `foo.c`
* Every `foo_test.c`-file contains a function like `void test_foo();` which is
  exposed through `test.h` and called inside the main function of `test.c`
* `test.h` also includes simple data structures for e.g. errors that can be used
  for writing tests
* Currently, there is no external unit test library, but we MIGHT use one later on

Although it is not necessary to get a test coverage of 100%, we should at least
try to automatically test as many cases as possible for error-prone components.
The lexer for example has to deal with a lot of maybe weird user input and thus
needs to be seriously tested.
